module com.zarbosoft.pidgooncommand {
	requires com.zarbosoft.interface1;
	requires com.zarbosoft.rendaw.common;
	requires com.google.common;
	requires com.zarbosoft.pidgoon;
	exports com.zarbosoft.pidgooncommand;
}