package com.zarbosoft.pidgooncommand;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.zarbosoft.interface1.Configuration;
import com.zarbosoft.interface1.OneshotTypeVisitor;
import com.zarbosoft.interface1.TypeInfo;
import com.zarbosoft.interface1.Walk;
import com.zarbosoft.pidgoon.AbortParse;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.events.*;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.internal.Callback;
import com.zarbosoft.pidgoon.nodes.Reference;
import com.zarbosoft.pidgoon.nodes.Repeat;
import com.zarbosoft.pidgoon.nodes.Sequence;
import com.zarbosoft.pidgoon.nodes.Union;
import com.zarbosoft.rendaw.common.ChainComparator;
import com.zarbosoft.rendaw.common.Pair;
import io.github.classgraph.ScanResult;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.zarbosoft.rendaw.common.Common.stream;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public class Command {
	// TODO show default values

	private static class Operator extends com.zarbosoft.pidgoon.nodes.Operator<StackStore> {
		public Operator(final Callback<StackStore> callback) {
			super(callback);
		}

		public Operator(
				final Node root, final Callback<StackStore> callback
		) {
			super(root, callback);
		}
	}

	/**
	 * Use in addition to @Configuration to annotate fields as arguments.
	 */
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Argument {
		/**
		 * -1 if not a positional argument, otherwise the unique position of the argument.  Positions do not need
		 * to be contiguous.
		 *
		 * @return
		 */
		int index() default -1;

		/**
		 * Optional short name for a keyword argument.
		 *
		 * @return
		 */
		String shortName() default "";

		/**
		 * Stop parsing when this argument is seen.
		 *
		 * @return
		 */
		boolean earlyExit() default false;

		/**
		 * @return
		 */
		String description() default "";
	}

	private static class ArgEvent implements MatchingEvent {
		public final String value;

		public ArgEvent(final String arg) {
			this.value = arg;
		}

		public ArgEvent() {
			this.value = null;
		}

		public boolean matches(final MatchingEvent event) {
			if (!(event instanceof ArgEvent))
				return false;
			if (value == null)
				return true;
			return value.equals(((ArgEvent) event).value);
		}

		@Override
		public String toString() {
			return value == null ? "*" : value;
		}
	}

	private static <T> Stream<Pair<Argument, Pair<TypeInfo, T>>> streamPositional(
			final TypeInfo klass, final List<Pair<TypeInfo, T>> fields
	) {
		final Set<Integer> seenIndices = new HashSet<>();
		return fields
				.stream()
				.map(pair -> new Pair<>(pair.first.field.getAnnotation(Argument.class), pair))
				.filter(pair -> pair.first != null && pair.first.index() >= 0)
				.sorted(new ChainComparator<Pair<Argument, Pair<TypeInfo, T>>>()
						.lesserFirst(pair -> pair.first.index())
						.build())
				.map(pair -> {
					final Configuration configuration = pair.second.first.field.getAnnotation(Configuration.class);
					if (configuration.optional())
						throw new AssertionError(String.format("Positional arguments must not be optional (%s in %s).",
								pair.second.first,
								klass
						));
					if (seenIndices.contains(pair.first.index()))
						throw new AssertionError(String.format("Multiple arguments with index %s in %s.",
								pair.first.index(),
								klass
						));
					return pair;
				});
	}

	private static <T> Stream<Pair<Argument, Pair<TypeInfo, T>>> streamKeyword(
			final TypeInfo klass, final List<Pair<TypeInfo, T>> fields
	) {
		final Set<String> seenKeywords = new HashSet<>();
		return fields
				.stream()
				.map(pair -> new Pair<>(pair.first.field.getAnnotation(Argument.class), pair))
				.filter(pair -> pair.first == null || pair.first.index() < 0)
				.map(pair -> {
					final Argument argument = pair.first;
					final TypeInfo field = pair.second.first;
					if (argument != null && !argument.shortName().isEmpty()) {
						if (seenKeywords.contains(argument.shortName()))
							throw new AssertionError(String.format("Duplicate keyword identifier [%s] in %s.",
									argument.shortName(),
									klass.type
							));
						seenKeywords.add(argument.shortName());
					}
					final String longName = Walk.decideName(field.field);
					if (seenKeywords.contains(longName))
						throw new AssertionError(String.format("Duplicate keyword identifier [%s] in %s.",
								longName,
								klass.type
						));
					return pair;
				});
	}

	public static void showHelp(
			final ScanResult reflections, final Class<?> rootClass, final String usagePrefix
	) {
		class Line {
			int indent;
			String text;

			public Line(final String text) {
				indent = 0;
				this.text = text;
			}

			public Line(final int indent, final String text) {
				this.indent = indent;
				this.text = text;
			}

			public Line indent() {
				return new Line(indent + 1, text);
			}
		}
		final List<Iterable<Line>> lines = new ArrayList<>();
		Walk.walkType(reflections, new TypeInfo(rootClass), null, new OneshotTypeVisitor<Void, Iterable<Line>>() {

			@Override
			public Iterable<Line> visitString(Void argument, final TypeInfo field) {
				return ImmutableList.of(new Line("Any string"));
			}

			@Override
			public Iterable<Line> visitSigned(Void argument, final TypeInfo field) {
				return ImmutableList.of(new Line("Any integer"));
			}

			@Override
			public Iterable<Line> visitUnsigned(Void argument, final TypeInfo field) {
				return ImmutableList.of(new Line("Any natural number"));
			}

			@Override
			public Iterable<Line> visitDouble(Void argument, final TypeInfo field) {
				return ImmutableList.of(new Line("Any double"));
			}

			@Override
			public Iterable<Line> visitBoolean(Void argument, final TypeInfo field) {
				return ImmutableList.of(new Line("true"), new Line("false"));
			}

			@Override
			public Iterable<Line> visitEnum(Void argument, final TypeInfo field) {
				return Walk
						.enumValues((Class<?>) field.type)
						.stream()
						.map(pair -> new Line(Walk.decideName(pair.second)))
						.collect(Collectors.toList());
			}

			@Override
			public Iterable<Line> visitListEnd(Void argument, final TypeInfo field, final Iterable<Line> inner) {
				return Iterables.concat(ImmutableList.of(new Line("(may be specified multiple times consecutively)")),
						inner
				);
			}

			@Override
			public Iterable<Line> visitSetEnd(Void argument, final TypeInfo field, final Iterable<Line> inner) {
				return Iterables.concat(ImmutableList.of(new Line("(may be specified multiple times consecutively)")),
						inner
				);
			}

			@Override
			public Iterable<Line> visitMapEnd(
					Void argument, final TypeInfo field, Iterable<Line> keyInner, final Iterable<Line> valueInner
			) {
				return Stream.of(Stream.of(new Line("KEY VALUE"), new Line("where KEY is:")),
						stream(keyInner).map(line -> line.indent()),
						Stream.of(new Line("where VALUE is:")),
						stream(valueInner).map(line -> line.indent())
				).flatMap(x -> x).collect(Collectors.toList());
			}

			@Override
			public Iterable<Line> visitAbstractEnd(
					Void argument, final TypeInfo field, final List<Pair<Class<?>, Iterable<Line>>> derived
			) {
				return derived.stream().map(pair -> new Line(Walk.decideName(pair.first))).collect(Collectors.toList());
			}

			@Override
			public Iterable<Line> visitConcreteShort(Void argument, final TypeInfo field) {
				return ImmutableList.of(new Line(Walk.decideName((Class<?>) field.type)));
			}

			@Override
			public void visitConcrete(
					Void argument_, final TypeInfo field, final List<Pair<TypeInfo, Iterable<Line>>> fields
			) {
				final Stream<Line> positionalStream = streamPositional(field, fields).flatMap(positional -> {
					Stream<Line> name =
							Stream.of(new Line(""), new Line(Walk.decideName(positional.second.first.field)));
					final Argument argument = positional.first;
					Stream<Line> description = argument == null || argument.description().isEmpty() ?
							Stream.empty() :
							Stream.<Line>of(new Line(argument.description()));
					Stream<Line> values = Stream.of(Stream.of(new Line("Value:")),
							stream(positional.second.second).<Line>map(line -> line.indent())
					).flatMap(l -> l);
					return Stream.of(name,
							Stream.of(new Line("")),
							Stream.concat(description, values).map(line -> line.indent())
					).flatMap(l -> l);
				}).map(line -> line.indent().indent());
				final boolean hasKeywords = streamKeyword(field, fields).anyMatch(x -> true);
				final Stream<Line> keywordStream = !hasKeywords ?
						Stream.empty() :
						Stream.of(Stream.of(new Line(""), new Line("KEYWORD ARGUMENTS")),
								streamKeyword(field, fields).flatMap(keyword -> {
									final Argument argument = keyword.first;
									final Field field2 = keyword.second.first.field;
									final Configuration configuration =
											uncheck(() -> field2.getAnnotation(Configuration.class));
									Stream<Line> name = Stream.of(new Line(""), new Line(String.format("%s%s%s",
											Walk.decideName(field2),
											(argument == null || argument.shortName().isEmpty()) ?
													"" :
													", " + argument.shortName(),
											configuration.optional() ? " (optional)" : ""
									)));
									Stream<Line> description = argument == null || argument.description().isEmpty() ?
											Stream.empty() :
											Stream.of(new Line(argument.description()));
									Stream<Line> values;
									if (field2.getType() == Boolean.class || field2.getType() == Boolean.TYPE) {
										values = null;
									} else if (keyword.second.second.iterator().hasNext())
										values = Stream.of(Stream.of(new Line("Value:")),
												stream(keyword.second.second).<Line>map(line -> line.indent())
										).flatMap(l -> l);
									else
										values = null;
									return Stream.<Stream<Line>>of(name,
											description == null && values == null ?
													Stream.empty() :
													Stream.of(Stream.of(new Line("")), (Stream<Line>) (
															description == null ? Stream.empty() : description
													), (Stream<Line>) (
															values == null ? Stream.empty() : values
													)).flatMap(l -> l).map(line -> line.indent())
									).flatMap(l -> l);
								}).map(line -> line.indent().indent())
						).flatMap(l -> l);
				lines.add(Stream.of(Stream.of(new Line(String.format("%s%s%s",
						field.type == rootClass ? usagePrefix : "",
						streamPositional(field, fields)
								.map(pair -> Walk.decideName(pair.second.first.field))
								.collect(Collectors.joining(" ")),
						hasKeywords ? " [keyword arguments]" : ""
				))), positionalStream, keywordStream).flatMap(l -> l).collect(Collectors.toList()));
			}
		});
		lines.forEach(iterable -> iterable.forEach(line -> System.out.format("%s%s\n",
				Strings.repeat("  ", line.indent),
				line.text
		)));
		System.out.flush();
	}

	public static <T> T parse(final ScanResult reflections, final Class<T> klass, final String[] args) {
		final Grammar grammar = new Grammar();
		grammar.add("root", Walk.walkType(reflections, new TypeInfo(klass), null, new OneshotTypeVisitor<Void, Node>() {
			@Override
			public Node visitString(Void argument, final TypeInfo field) {
				return new Operator(new MatchingEventTerminal(new ArgEvent()), store -> {
					return store.pushStack(((ArgEvent) store.top()).value);
				});
			}

			@Override
			public Node visitSigned(Void argument, final TypeInfo field) {
				return new Operator(new MatchingEventTerminal(new ArgEvent()), store -> {
					final ArgEvent event = (ArgEvent) store.top();
					try {
						return store.pushStack(Long.parseLong(event.value));
					} catch (final NumberFormatException e) {
						throw new AbortParse(String.format("%s is not an integer.", event.value));
					}
				});
			}

			@Override
			public Node visitUnsigned(Void argument, final TypeInfo field) {
				return new Operator(new MatchingEventTerminal(new ArgEvent()), store -> {
					final ArgEvent event = (ArgEvent) store.top();
					try {
						return store.pushStack(Long.parseUnsignedLong(event.value));
					} catch (final NumberFormatException e) {
						throw new AbortParse(String.format("%s is not an integer.", event.value));
					}
				});
			}

			@Override
			public Node visitDouble(Void argument, final TypeInfo field) {
				return new Operator(new MatchingEventTerminal(new ArgEvent()), store -> {
					final ArgEvent event = (ArgEvent) store.top();
					try {
						return store.pushStack(Double.parseDouble(event.value));
					} catch (final NumberFormatException e) {
						throw new AbortParse(String.format("%s is not a double.", event.value));
					}
				});
			}

			@Override
			public Node visitBoolean(Void argument, final TypeInfo field) {
				return new Operator(new MatchingEventTerminal(new ArgEvent()), store -> {
					final ArgEvent event = (ArgEvent) store.top();
					try {
						return store.pushStack(Boolean.parseBoolean(event.value));
					} catch (final NumberFormatException e) {
						throw new AbortParse(String.format("%s is not a boolean.", event.value));
					}
				});
			}

			@Override
			public Node visitEnum(Void argument, final TypeInfo field) {
				final Union union = new Union();
				Walk.enumValues((Class<?>) field.type).stream().forEach(pair -> {
					union.add(new Operator(new MatchingEventTerminal(new ArgEvent(Walk.decideName(pair.second))),
							store -> {
								return store.pushStack(pair.first);
							}
					));
				});
				return union;
			}

			@Override
			public Node visitListEnd(Void argument, final TypeInfo field, final Node inner) {
				return inner;
			}

			@Override
			public Node visitSetEnd(Void argument, final TypeInfo field, final Node inner) {
				return inner;
			}

			@Override
			public Node visitMapEnd(Void argument, final TypeInfo field, final Node innerKey, final Node innerValue) {
				return new Sequence()
						.add(new MatchingEventTerminal(new ArgEvent()))
						.add(new Operator(new MatchingEventTerminal(new ArgEvent()), store -> {
							return store.pushStack(Double.parseDouble(((ArgEvent) store.top()).value));
						}));
			}

			@Override
			public Node visitAbstractEnd(
					Void argument, final TypeInfo field, final List<Pair<Class<?>, Node>> derived
			) {
				final Union union = new Union();
				derived.forEach(pair -> union.add(new Sequence()
						.add(new MatchingEventTerminal(new ArgEvent(Walk.decideName(pair.first))))
						.add(pair.second)));
				return union;
			}

			@Override
			public Node visitConcreteShort(Void argument, final TypeInfo field) {
				return new Reference(field.type);
			}

			@Override
			public void visitConcrete(Void argument_, final TypeInfo field, final List<Pair<TypeInfo, Node>> fields) {
				final Union root = new Union();
				final Sequence positional = new Sequence();
				streamPositional(field, fields).forEach(pair -> {
					positional.add(new Operator(pair.second.second, store -> {
						store = store.pushStack(pair.second.first);
						return StackStore.stackDoubleElement(store);
					}));
				});
				final com.zarbosoft.pidgoon.nodes.Set keyword = new com.zarbosoft.pidgoon.nodes.Set();
				streamKeyword(field, fields).forEach(pair -> {
					final Argument argument = pair.first;
					final TypeInfo field2 = pair.second.first;
					final Configuration configuration = field2.field.getAnnotation(Configuration.class);
					final Node node = pair.second.second;
					final Union union = new Union();
					final List<Node> prefixes = new ArrayList<>();
					if (argument != null && !argument.shortName().isEmpty()) {
						prefixes.add(new MatchingEventTerminal(new ArgEvent(argument.shortName())));
					}
					final String longName = Walk.decideName(field2.field);
					prefixes.add(new MatchingEventTerminal(new ArgEvent(longName)));
					for (final Node prefix : prefixes) {
						if (field2.field.getType() == Boolean.class || field2.field.getType() == Boolean.TYPE)
							union.add(new Operator(prefix, store -> {
								store = store.pushStack(true);
								store = store.pushStack(field2);
								return StackStore.stackDoubleElement(store);
							}));
						else
							union.add(new Sequence().add(prefix).add(new Operator(node, store -> {
								store = store.pushStack(field2);
								return StackStore.stackDoubleElement(store);
							})));
					}
					final Node out;
					if (Collection.class.isAssignableFrom(field2.field.getType())) {
						out = new Repeat(union).min(1);
					} else if (Map.class.isAssignableFrom(field2.field.getType())) {
						out = new Repeat(union).min(1);
					} else
						out = union;
					if (argument != null && argument.earlyExit())
						root.add(out);
					else
						keyword.add(out, !configuration.optional());
				});
				positional.add(keyword);
				root.add(positional);
				final Node rule =
						new Sequence().add(new Operator(store -> store.pushStack(0))).add(new Operator(root, store -> {
							final Object out = uncheck(((Class<?>) field.type)::newInstance);
							final java.util.Set<Field> fields2 = new HashSet<>();
							fields2.addAll(fields.stream().map(pair -> pair.first.field).collect(Collectors.toList()));
							store = StackStore.<Pair<Object, TypeInfo>>stackPopSingleList(store, (pair) -> {
								Field outField = pair.second.field;
								fields2.remove(pair.second);
								uncheck(() -> {
									if (Collection.class.isAssignableFrom(outField.getType())) {
										Collection collection = (Collection) outField.get(out);
										if (collection == null) {
											if (List.class.isAssignableFrom(outField.getType()))
												collection = new ArrayList();
											else if (Set.class.isAssignableFrom(outField.getType()))
												collection = new HashSet();
											else
												throw new AssertionError(String.format("Can't handle collection type %s.",
														outField.getType()
												));
											outField.set(out, collection);
										}
										collection.add(pair.first);
									} else if (Map.class.isAssignableFrom(outField.getType())) {
										Map map = (Map) outField.get(out);
										if (map == null) {
											map = new HashMap();
											outField.set(out, map);
										}
										map.put(((Pair<String, Object>) pair.first).first,
												((Pair<String, Object>) pair.first).second
										);
									} else if (outField.getType() == byte.class || outField.getType() == Byte.class) {
										outField.set(out, (byte) (long) pair.first);
									} else if (outField.getType() == int.class || outField.getType() == Integer.class) {
										outField.set(out, (int) (long) pair.first);
									} else
										outField.set(out, pair.first);
								});
							});
							for (final Field field2 : out.getClass().getFields()) {
								if (field2.getAnnotation(Configuration.class) == null)
									continue;
								if (!List.class.isAssignableFrom(field2.getType()))
									continue;
								final List value = uncheck(() -> (List<?>) field2.get(out));
								if (value != null)
									Collections.reverse(value);
							}
							return store.pushStack(out);
						}));
				grammar.add(field.type, rule);
			}
		}));
		EventStream<T> stream = new Parse<T>().grammar(grammar).parse();
		for (int i = 0; i < args.length; ++i)
			stream = stream.push(new ArgEvent(args[i]), String.format("arg %s", i + 1));
		return stream.result();
	}
}
